/* Implemented in l.s */
extern int getpc(void);
extern int getsp(void);
extern int getsc(void);
extern int getcpsr(void);
extern int getspsr(void);
extern int getapsr(void);
extern int getfpscr(void);
extern int getcontrol(void);
extern void setcontrol(int);
extern int getR12(void);
extern int getmsp(void);
extern int getpsp(void);
extern void setmsp(int);
extern void setpsp(int);

extern void intron(void);
extern void introff(void);

/* Implemented in sys.c */
extern int getcpacr(void);
extern int getcpuid(void);
extern void enablefpu(void);
extern int getshcsr(void);
extern short getufsr(void);
extern short clearufsr(short);
extern int getfpccr(void);
extern int getccr(void);
extern int getcfsr(void);
extern int setcfsr(int bits);
