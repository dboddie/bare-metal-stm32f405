#include "../stm32f405.h"
#include "../thumb2_fns.h"

void systick(void)
{
}

#define SHOWF(X, E) \
    wrhex(*(int *)(&X)); wrch(' '); wrsfp(X); wrch(' '); wrstr(E); newline();

void main(void)
{
    enablefpu();
    setup_system_clock();
    setup_usart();

    float x = 0.1;
    float y = 0.2;

    SHOWF(x, "0.1")
    SHOWF(y, "0.2")

    wrstr("x == y ? "); wrdec(x == y); newline();
    wrstr("x != y ? "); wrdec(x != y); newline();
    wrstr("x < y ? "); wrdec(x < y); newline();
    wrstr("x <= y ? "); wrdec(x <= y); newline();
    wrstr("x > y ? "); wrdec(x > y); newline();
    wrstr("x >= y ? "); wrdec(x >= y); newline();

    for (;;) {
    }
}
