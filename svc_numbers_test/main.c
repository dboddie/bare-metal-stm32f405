#include "../thumb2.h"
#include "../thumb2_fns.h"
#include "../stm32f405.h"

/* Writes an asterisk or plus symbol to the USART. */
void systick(void)
{
}

/* Changes the symbol written to the USART. */
void svcall(int svc_number)
{
    wrdec(svc_number);
    newline();
}

extern void svc0(void);

void main(void)
{
    setup_system_clock();
    setup_usart();

    int i = 0;
    for (;; i++) {
        /* Call a syscall every so often. */
        if (i % 5000000 == 0)
            svc0();
        else if (i % 5000000 == 2500000)
            svc42();
    }
}
