#include "../thumb2.h"
#include "vectors.s"

THUMB=4
STACK_TOP=0x20020000

TEXT _start(SB), THUMB, $-4

    MOVW    $setR12(SB), R1
    MOVW    R1, R12	/* static base (SB) */
    MOVW    $STACK_TOP, R1
    MOVW    R1, SP

    /* Copy initial values of data from after the end of the text section to
       the beginning of the data section. */
    MOVW    $etext(SB), R1
    MOVW    $bdata(SB), R2
    MOVW    $edata(SB), R3

_start_loop:
    CMP     R3, R2              /* Note the reversal of the operands */
    BGE     _end_start_loop

    MOVW    (R1), R4
    MOVW    R4, (R2)
    ADD     $4, R1
    ADD     $4, R2
    B       _start_loop

_end_start_loop:

    B   ,main(SB)

TEXT _dummy(SB), THUMB, $-4

    B   ,_dummy(SB)

#define SYSTICK_CR 0xe000e010
#define SYSTICK_AHB 4
#define SYSTICK_EXC 2
#define SYSTICK_EN 1

TEXT _svcall(SB), THUMB, $-4
TEXT _systick(SB), THUMB, $-4

    /* In handler mode; R0-R3, R12, R14, PC from the preempted code are saved
       on the stack. */

    PUSH(0xff0, 1)              /* Push R4-11 and R14 onto the stack. */

    MOVW    SP, R0              /* Pass the stack pointer to the switcher. */
    BL      ,switcher(SB)
    MOVW    R0, SP              /* Obtain the new stack pointer to use. */

    POP(0xff0, 1)

TEXT _hard_fault(SB), THUMB, $-4
    MRS(0, MRS_MSP)                 /* Assuming MSP not PSP, save SP before
                                       usage_fault changes it. */

    BL ,hard_fault(SB)

TEXT getR12(SB), THUMB, $-4
    MOVW R12, R0
    RET

TEXT getsp(SB), THUMB, $-4
    MOVW    SP, R0
    RET

TEXT svc0(SB), THUMB, $-4
    SVC(0)
    RET
