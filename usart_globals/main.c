#include "../stm32f405.h"

void systick(void)
{
}

extern char etext[];
extern char bdata[];
extern char edata[];
extern char end[];
int x = 123;

void main(void)
{
    setup_system_clock();
    setup_usart();

    wrstr("etext="); wrhex((int)etext); wrch(' ');
    wrstr("bdata="); wrhex((int)bdata); wrch(' ');
    wrstr("edata="); wrhex((int)edata); wrch(' ');
    wrstr("end="); wrhex((int)end); newline();

    /* Read the initial value of x from the data section to test that it was
       copied. */
    write_dec(x); newline();
    /* Write to x to check that it is held in RAM. */
    x = 42;
    write_dec(x); newline();

    for (;;) {
    }
}
