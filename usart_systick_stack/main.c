#include "../stm32f405.h"

void systick(int msp)
{
    wrstr("pc="); wrhex(*(int *)(msp + 24)); newline();
}

void main(void)
{
    setup_system_clock();
    setup_usart();
    start_timer();

    for (;;) {
    }
}
