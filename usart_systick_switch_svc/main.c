#include "../stm32f405.h"
#include "../thumb2.h"
#include "../thumb2_fns.h"

void pause_timer(void)
{
    SysTick *tick = (SysTick *)SYSTICK;
    tick->control &= ~1;    /* 1=enable */
}

void unpause_timer(void)
{
    SysTick *tick = (SysTick *)SYSTICK;
    tick->control |= 1;     /* 1=enable */
}

typedef struct
{
    unsigned int pc;
    unsigned int sp;
    unsigned int begun;
} Process;

unsigned int switch_pc = 0;

Process processes[2];
int current_process;

void dowork(int c)
{
    wrhex(c); wrstr(" sp="); wrhex(getsp()); newline();
    for (int i = 0; i < 300000; i++);
}

void process0(void)
{
    int c = 0;
    for (;;) {
        wrstr("process 0:"); wrstr(" sp="); wrhex(getsp()); newline();
        dowork(c++);
    }
}

void process1(void)
{
    int c = 0;
    for (;;) {
        wrstr("process 1:"); wrstr(" sp="); wrhex(getsp()); newline();
        dowork(c++);
    }
}

/* The stack during an exception (without FP registers) */
typedef struct
{
    unsigned int r0;
    unsigned int r1;
    unsigned int r2;
    unsigned int r3;
    unsigned int r12;
    unsigned int r14;
    unsigned int pc;
    unsigned int psr;
} Regs;

void dumpregs(unsigned int sp)
{
    int r14 = *(int *)sp;
    for (int i = 0; i < 13; i++) {
        wrch('r'); wrdec(i); wrch('='); wrhex(*(int *)(sp + 4 + (i * 4))); newline();
    }
    wrstr("r14="); wrhex(r14); newline();
    wrstr("pc="); wrhex(*(int *)(sp + 56)); newline();
}

unsigned int switcher(unsigned int sp)
{
//    wrstr("> pc="); wrhex(switch_pc); wrstr(" sp="); wrhex(sp); newline();
//    dumpregs(sp);

    /* Store the preempted PC on the stack for recovery when this process
       is resumed. */
    *(unsigned int *)(sp + 56) = switch_pc;

    if (current_process != -1) {
        /* Store the stack pointer for later recovery, and record the PC purely
           for information. */
        processes[current_process].sp = sp;
        processes[current_process].pc = switch_pc;
        /* Switch between the two processes. */
        current_process = 1 - current_process;
    } else
        current_process = 0;

    sp = processes[current_process].sp;

    Process *p = &processes[current_process];
    if (!p->begun) {
        sp -= 56;
        *(unsigned int *)(sp + 56) = p->pc;
        p->begun = 1;
    }

//    wrstr("< pc="); wrhex(switch_pc); wrstr(" sp="); wrhex(sp); newline();
//    dumpregs(sp);
    return sp;
}

void hard_fault(unsigned int sp)
{
    wrstr("Hard fault\r\n");
    wrstr("HFSR="); wrhex(*(int *)HFSR_ADDR); newline();
    wrstr("pc="); wrhex(*(int *)(sp + 24)); newline();
    for (;;) {}
}

void initproc(Process *p, unsigned int addr, unsigned int sp)
{
    p->pc = addr;
    p->sp = sp;
    p->begun = 0;
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    initproc(&processes[0], (unsigned int)&process0, 0x20010000);
    initproc(&processes[1], (unsigned int)&process1, 0x20011000);

    current_process = -1;
    start_timer();

    for (;;) {
        wrch('.');
    }
}
