#include "../stm32f405.h"

void start_timer(void)
{
    SysTick *tick = (SysTick *)SYSTICK;
    /* The system clock is 16MHz, so set a reset value for 1s. */
    tick->reload = 16000000 - 1;
    tick->current = 0;
    tick->control = 7;  /* 4=processor clock (0=AHB/8, 4=AHB),
                           2=SysTick exception, 1=enable */
}

void systick(void)
{
    GPIO *gpioa = (GPIO *)GPIO_A;

    gpioa->odr ^= 1 << 15;
}

void divide_AHB1_clock(int power_of_two)
{
    uint old = *(uint *)RCC_CFGR;
    uint v = power_of_two + 7;
    *(uint *)RCC_CFGR = (old & 0xffffff0f) | ((v & 0xf) << 4);
}

void main(void)
{
    RCC *rcc = (RCC *)RCC_CR;

    /* HSI=16 MHz, divide by 8, multiply by 168, divide by 2, /7 for USB clock */
    rcc->pllcfgr = (RCC_PLL_HSI_Source << 22) |
                   (8 << 0) | (168 << 6) | (0 << 16) | (7 << 24);

    rcc->cr |= RCC_PLLON;
    while ((rcc->cr & RCC_PLLRDY) != RCC_PLLRDY);

    /* Divide the clock for the AHB clock (168 MHz /4 = 64 MHz) */
    divide_AHB1_clock(2);

    rcc->cfgr = (rcc->cfgr & ~3) | RCC_CFGR_SW_PLL;
    while ((rcc->cfgr & 3) != RCC_CFGR_SW_PLL);

    /* The GPIO pins are on the AHB bus and must be enabled */
    rcc->ahb1enr |= RCC_AHB1_ENABLE_GPIO_A;

    GPIO *gpioa = (GPIO *)GPIO_A;
    /* Set the pin mode using the pair of bits for PA15: */
    gpioa->moder = GPIO_Output << 30;
    /* Set the speed */
    gpioa->ospeedr = GPIO_HighSpeed << 30;
    /* Turn on the status LED */
    gpioa->odr = 1 << 15;

    start_timer();

    for (;;) {
    }
}
