#include "../stm32f405.h"
#include "../thumb2.h"
#include "../thumb2_fns.h"

void pause_timer(void)
{
    SysTick *tick = (SysTick *)SYSTICK;
    tick->control &= ~1;    /* 1=enable */
}

void unpause_timer(void)
{
    SysTick *tick = (SysTick *)SYSTICK;
    tick->control |= 1;     /* 1=enable */
}

typedef struct
{
    unsigned long pc;
    unsigned long sp;
    unsigned long begun;
} Process;

unsigned int switch_pc = 0;

Process processes[2];
int current_process;

void dowork(int c)
{
    wrhex(c); wrstr(" sp="); wrhex(getsp()); newline();
    for (int i = 0; i < 300000; i++);
}

void process0(void)
{
    int c = 0;
    for (;;) {
        wrstr("process 0:"); wrstr(" sp="); wrhex(getsp()); newline();
        dowork(c++);
    }
}

void process1(void)
{
    int c = 0;
    for (;;) {
        wrstr("process 1:"); wrstr(" sp="); wrhex(getsp()); newline();
        dowork(c++);
    }
}

/* The stack during an exception (without FP registers) */
typedef struct
{
    unsigned long r4;
    unsigned long r5;
    unsigned long r6;
    unsigned long r7;
    unsigned long r8;
    unsigned long r9;
    unsigned long r10;
    unsigned long r11;
    unsigned long exc_r14;
    unsigned long r0;
    unsigned long r1;
    unsigned long r2;
    unsigned long r3;
    unsigned long r12;
    unsigned long r14;
    unsigned long pc;
    unsigned long psr;
} Ureg;

void dumpregs(Ureg *uregs)
{
    wrstr("r0="); wrhex(uregs->r0); wrch(' ');
    wrstr("r1="); wrhex(uregs->r1); wrch(' ');
    wrstr("r2="); wrhex(uregs->r2); wrch(' ');
    wrstr("r3="); wrhex(uregs->r3); newline();
    wrstr("r4="); wrhex(uregs->r4); wrch(' ');
    wrstr("r5="); wrhex(uregs->r5); wrch(' ');
    wrstr("r6="); wrhex(uregs->r6); wrch(' ');
    wrstr("r7="); wrhex(uregs->r7); newline();
    wrstr("r8="); wrhex(uregs->r8); wrch(' ');
    wrstr("r9="); wrhex(uregs->r9); wrch(' ');
    wrstr("r10="); wrhex(uregs->r10); wrch(' ');
    wrstr("r11="); wrhex(uregs->r11); newline();
    wrstr("r12="); wrhex(uregs->r12); wrch(' ');
    wrstr("sp="); wrhex((unsigned long)uregs); wrch(' ');
    wrstr("r14="); wrhex(uregs->r14); newline();
    wrstr("pc="); wrhex(uregs->pc); newline();
    wrstr("exc_return="); wrhex(uregs->exc_r14); wrch(' ');
    wrstr("psr="); wrhex(uregs->psr); newline();
}

unsigned long switcher(Ureg *ureg)
{
    unsigned long sp = (unsigned long)ureg;
    Ureg *old_ureg = ureg;

//    wrstr("> pc="); wrhex(ureg->pc); wrstr(" sp="); wrhex(sp); newline();
//    dumpregs(ureg);

    if (current_process != -1) {
        /* Store the stack pointer for later recovery, and record the PC purely
           for information. */
        processes[current_process].sp = sp;
        processes[current_process].pc = ureg->pc;
        /* Switch between the two processes. */
        current_process = 1 - current_process;
    } else
        current_process = 0;

    Process *p = &processes[current_process];
    sp = p->sp;

    if (!p->begun) {
        /* Push a starting set of registers onto the stack. */
        sp -= sizeof(Ureg);
        ureg = (Ureg *)sp;
        ureg->pc = p->pc;
        ureg->psr = 0x01000000;
        ureg->exc_r14 = old_ureg->exc_r14;
        ureg->r12 = old_ureg->r12;
        p->begun = 1;
        p->sp = sp;
    }

//    ureg = (Ureg *)sp;
//    wrstr("< pc="); wrhex(ureg->pc); wrstr(" sp="); wrhex(sp); newline();
//    dumpregs(ureg);
    return sp;
}

void hard_fault(unsigned int sp)
{
    wrstr("Hard fault\r\n");
    wrstr("HFSR="); wrhex(*(int *)HFSR_ADDR); newline();
    wrstr("pc="); wrhex(*(int *)(sp + 24)); newline();
    for (;;) {}
}

void initproc(Process *p, unsigned long addr, unsigned long sp)
{
    wrstr("initproc "); wrhex(addr); wrch(' '); wrhex(sp); newline();
    p->pc = addr;
    p->sp = sp;
    p->begun = 0;
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    initproc(&processes[0], (unsigned long)(&process0), 0x20010000);
    initproc(&processes[1], (unsigned long)&process1, 0x20011000);

    current_process = -1;
    start_timer();

    for (;;) {
        wrch('.');
        for (int i = 0; i < 100000; i++);
    }
}
