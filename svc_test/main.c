#include "../thumb2.h"
#include "../thumb2_fns.h"
#include "../stm32f405.h"

/* Writes an asterisk or plus symbol to the USART. */
void systick(void)
{
    int ch = *(int *)0x20002000;
    wrch(42 + ch);
}

/* Changes the symbol written to the USART. */
void svcall(void)
{
    *(int *)0x20002000 ^= 1;
}

extern void svc0(void);

void main(void)
{
    setup_system_clock();
    setup_usart();

    /* Initialise the offset to use when writing symbols. */
    *(int *)0x20002000 = 0;
    start_timer();

    int i = 0;
    for (;; i++) {
        /* Call a syscall every so often. */
        if (i % 5000000 == 0)
            svc0();
    }
}
