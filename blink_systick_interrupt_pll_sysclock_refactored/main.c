#include "../stm32f405.h"

void systick(void)
{
    toggle_led();
}

void main(void)
{
    setup_system_clock();

    setup_LED();
    set_LED(1);

    start_timer();

    for (;;) {
    }
}
