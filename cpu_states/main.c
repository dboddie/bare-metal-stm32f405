#include "../thumb2.h"
#include "../thumb2_fns.h"
#include "../stm32f405.h"

void systick(void)
{
    wrch('*');
}

void main(void)
{
    setup_system_clock();
    setup_usart();
    start_timer();

    int i = 0;
    int on = 1;
    for (;; i++) {
        if ((i % 5000000) == 0) {
            if (on) {
                wrstr("Interrupts off...\r\n");
                introff();
            } else 
            {
                intron();
                wrstr("Interrupts on again\r\n");
            }
            on = 1 - on;
        }
    }
}
