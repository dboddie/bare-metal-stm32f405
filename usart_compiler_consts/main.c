#include "../stm32f405.h"
#include "../thumb2_fns.h"

void systick(void)
{
}

extern int setR12;
extern	char	bdata[];
extern	char	etext[];
extern	char	edata[];
extern	char	end[];

void main(void)
{
    enablefpu();
    setup_system_clock();
    setup_usart();

    wrstr("SB="); wrhex(getR12()); newline();
    wrstr("bdata="); wrhex((int)bdata); newline();
    wrstr("etext="); wrhex((int)etext); newline();
    wrstr("edata="); wrhex((int)edata); newline();
    wrstr("end="); wrhex((int)edata); newline();
    wrstr("setR12="); wrhex(setR12); newline();

    for (;;) {
    }
}
