#include "../stm32f405.h"

typedef	struct	Vlong	Vlong;
struct	Vlong
{
	union
	{
		struct
		{
			unsigned long	hi;
			unsigned long	lo;
		};
		struct
		{
			unsigned short	hims;
			unsigned short	hils;
			unsigned short	loms;
			unsigned short	lols;
		};
	};
};

long
_v2si(Vlong rv)
{

	return rv.lo;
}

void systick(void)
{
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    long long a = 32, b = 11;
    long long c = a * b;
    wrdecll(a); wrch(32); wrdecll(b); wrch(32); wrdecll(c); newline();
    a = -15;
    c = a * b;
    wrdecll(a); wrch(32); wrdecll(b); wrch(32); wrdecll(c); newline();

    for (;;) {
    }
}
