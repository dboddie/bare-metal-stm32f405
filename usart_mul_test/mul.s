THUMB = 4

/* ARM Architecture Reference Manual Thumb-2 Supplement, 4.6.43, T3 */
#define LDRimm(Rt,Rn,imm12) \
    WORD $(0x0000f8d0 | Rn | (Rt<<28) | ((imm12 & 0xfff)<<16))

/* ARM Architecture Reference Manual Thumb-2 Supplement, 4.6.207, T1 */
#define UMULL(Rn,Rm,RdHi,RdLo) \
    WORD $(0x0000fba0 | Rn | (Rm<<16) | (RdHi<<24) | (RdLo<<28))

/* ARM Architecture Reference Manual Thumb-2 Supplement, 4.6.84, T2 */
#define	MUL(Rn,Rm,Rd) \
    WORD $(0xf000fb00 | Rn | (Rm<<16) | (Rd<<24))

arg=0

/* replaced use of R10 by R11 because the former can be the data segment base register */

TEXT	_mulv(SB), THUMB, $0
	MOVW	4(FP), R2	/* l0 */
	MOVW	8(FP), R3	/* h0 */
	MOVW	12(FP), R4	/* l1 */
	MOVW	16(FP), R5	/* h1 */
	UMULL(4, 2, 7, 6)
	MUL(3, 4, 8)
	ADD	R8, R7
	MUL(2, 5, 8)
	ADD	R8, R7
	MOVW	R6, 0(R(arg))
	MOVW	R7, 4(R(arg))
	RET
