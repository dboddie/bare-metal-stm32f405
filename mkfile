default:V:	-sh
clean:V:	clean-sh

DIRS=\
	blink_hse\
	blink_hse\
	blink_hse_pll\
	blink_systick\
	blink_systick_interrupt\
	blink_systick_interrupt_clock_divide\
	blink_systick_interrupt_pll_sysclock\
	blink_systick_interrupt_pll_sysclock_refactored\
	blink_hse\
	blink_hse_pll\
	cpu_states\
	i2c_temperature\
	svc_test\
	blink_systick\
	blink_systick_interrupt\
	blink_systick_interrupt_clock_divide\
	blink_systick_interrupt_pll_sysclock\
	blink_systick_interrupt_pll_sysclock_refactored\
	system_info\
	usart_compiler_consts\
	usart_constants\
	usart_div_test\
	usart_double\
	usart_float_arithmetic\
	usart_float_compare_test\
	usart_float_test\
	usart_globals\
	usart_string_test\

%-sh:QV:
	for d in $DIRS
	do
	  if test -d $d; then
	    echo "(cd $d; mk $MKFLAGS $stem)"
	    (cd $d; mk $stem) || exit 1
	  fi
	done
