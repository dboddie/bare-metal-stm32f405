#include "../stm32f405.h"

void systick(void)
{
}

extern	char	etext[];
extern	char	edata[];
extern	char	end[];

extern int getR12(void);
extern int getsp(void);

void main(void)
{
    for (char *a = edata; a < end; a++)
        *a = '0';

    setup_system_clock();

    setup_usart();
/*
    start_timer();
*/
    wrhex((int)etext); newline();
    wrhex((int)edata); newline();
    wrhex((int)end); newline();
    wrhex(getR12()); newline();
    wrhex(getsp()); newline();
    wrhex((int)(const char *)"Hello\r\n"); newline();
    wrstr("Hello\r\n");

    const char s[] = "World\r\n";
    wrstr(s);

    for (;;) {
    }
}
