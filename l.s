#include "thumb2.h"
#include "vectors.s"

THUMB=4
STACK_TOP=0x20020000

TEXT _start(SB), THUMB, $-4

    MOVW    $setR12(SB), R1
    MOVW    R1, R12	/* static base (SB) */
    MOVW    $STACK_TOP, R1
    MOVW    R1, SP

    /* Copy initial values of data from after the end of the text section to
       the beginning of the data section. */
    MOVW    $etext(SB), R1
    MOVW    $bdata(SB), R2
    MOVW    $edata(SB), R3

_start_loop:
    CMP     R3, R2              /* Note the reversal of the operands */
    BGE     _end_start_loop

    MOVW    (R1), R4
    MOVW    R4, (R2)
    ADD     $4, R1
    ADD     $4, R2
    B       _start_loop

_end_start_loop:

    B   ,main(SB)

TEXT _dummy(SB), THUMB, $-4

    B   ,_dummy(SB)

TEXT _systick(SB), THUMB, $0

    BL  ,systick(SB)
    RET

TEXT getR12(SB), THUMB, $-4
    MOVW R12, R0
    RET

TEXT getsp(SB), THUMB, $-4
    MOVW    SP, R0
    RET

TEXT getpc(SB), THUMB, $-4
    MOVW    R14, R0
    RET

TEXT getsc(SB), THUMB, $-4
    MRC     CpSC, 0, R0, C(CpCONTROL), C(0), CpMainctl
    RET

TEXT getspsr(SB), THUMB, $-4
    /* Read SPSR into R0 */
    WORD    $(0x8000f3ef | (1 << 4) | (0 << 24))
    RET

TEXT getapsr(SB), THUMB, $-4
    /* Read APSR into R0 */
    WORD    $(0x8000f3ef | (0 << 4) | (0 << 24))
    RET

TEXT getfpscr(SB), THUMB, $-4
    VMRS(0)
    RET

TEXT getcontrol(SB), THUMB, $-4
    MRS(0, 20)
    RET

TEXT testfp(SB), THUMB, $-4
    CMPF    F2, F3
/*
    MOVW    $0x20000000, R0
    MOVF    (R0),F1
    MOVF    (SP),F2
    MOVF    4(SP),F3
    MOVF    (PC),F4
    MOVF    0x210,F5
    RET

DATA    some_value+0(SB)/4, $0x12345678;
GLOBL   some_value(SB), $4
*/

/* Dumps some registers into memory for debugging, corrupts R0 */
TEXT dumptmp(SB), THUMB, $-4
    MOVW R3, 0(R0)
    MOVW R7, 4(R0)
    MOVW R12, R3
    MOVW R3, 8(R0)
    MOVW SP, R3
    MOVW R3, 12(R0)
    MOVW 0(R0), R3
    RET

TEXT dumpfloat(SB), THUMB, $-4
    MOVW 4(SP), R0
    RET

TEXT introff(SB), THUMB, $-4
    CPS(1, CPS_I)
    RET

TEXT intron(SB), THUMB, $-4
    CPS(0, CPS_I)
    RET
