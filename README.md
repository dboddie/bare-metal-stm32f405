# Bare Metal Tests for the STM32F405 MCU using the Inferno C Compiler

This repository contains some test programs for the Thumb C compiler provided
by <a href="https://www.vitanuova.com/inferno/">Inferno</a>. They were
written specifically for the
<a href="https://www.sparkfun.com/products/17713">MicroMod STM32 Processor
board</a> but could be adapted for other hardware.

## Building the Tests

Each of the tests has its own directory. Common files are in the root of the
repository.

Make sure you have the directory containing the Inferno executables in your
`PATH` environment variable.

Try building a test by entering the `blink_systick` directory and typing `mk`
to build an executable called `out.bin`:

    cd blink_systick
    mk

I use the `stm32flash` tool to program the STM32F405 on the MicroMod board.
The device usually appears as `/dev/ttyUSB0` so this command works for me:

    stm32flash -R -w out.bin /dev/ttyUSB0

The `-R` option causes the board to reset after programming. The status LED on
the board should blink on/off every second or so.

You can also build most of the tests by running `mk` in the root directory of
the repository.

## Copyright and License Information

Since this code was written to enable others to learn and experiment, the
license used for the code aims to be as generous as possible.

    Written in 2021 by David Boddie <david@boddie.org.uk>

    To the extent possible under law, the author(s) have dedicated all copyright
    and related and neighboring rights to this software to the public domain
    worldwide. This software is distributed without any warranty.

    You should have received a copy of the CC0 Public Domain Dedication along with
    this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

More information can be found in the `COPYING` file which was obtained
[from the Creative Commons website](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt).
