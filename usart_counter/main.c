#include "../stm32f405.h"

int counter = 0;

void systick(void)
{
    counter++;
    if (counter % 100 == 0) {
        wrdec(counter);
        newline();
    }
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    SysTick *tick = (SysTick *)SYSTICK;
    /* The scaled system clock is 42MHz, so set a reset value for 0.25s. */
    tick->reload = 420000 - 1;
    tick->current = 0;
    tick->control = 7;  /* 4=processor clock (0=AHB/8, 4=AHB),
                           2=SysTick exception, 1=enable */
    for (;;) {
    }
}
