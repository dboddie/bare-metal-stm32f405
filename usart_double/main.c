#include "../thumb2.h"
#include "../stm32f405.h"
#include "../thumb2_fns.h"

void systick(void)
{
}

void showd(double x, char *s)
{
    int addr = (int)(&x);
    wrhex(addr); newline();
    wrhex(addr + 4); newline();
    wrhex(*(int *)(addr + 4)); wrhex(*(int *)addr); newline();
    wrstr(s); newline();
}

void usage_fault(int msp)
{
    wrstr("Instruction: "); wrhex(**(int **)(msp + 24)); newline();
    *(int *)(msp + 24) += 4;
    *(int *)(msp + 28) = 0x01000000;
}

void main(void)
{
    enablefpu();
    setup_system_clock();
    setup_usart();

    /* Enable the usage fault exception. */
    *(int *)SHCSR_ADDR |= SHCSR_USGFAULTENA | SHCSR_MEMFAULTENA;

    wrstr("CCR="); wrhex(getccr()); newline();
    /* Bit 9 (STKALIGN) appears to be set by default. */

    wrstr("FPCCR="); wrhex(getfpccr()); newline();
    /* This appears to have ASPEN and LSPEN set by default. */

    wrstr("sizeof(double) = "); wrdec(sizeof(double)); newline();

    wrstr("R12="); wrhex(getR12()); newline();
    wrstr("MSP="); wrhex(getmsp()); newline();
    wrstr("PSP="); wrhex(getpsp()); newline();
    double x = 1.0;
//    showd(x, "1.0");

    wrstr("Skipped\r\n");
    x = 2.0;

    wrstr("End\r\n");

    for (;;) {
    }
}
