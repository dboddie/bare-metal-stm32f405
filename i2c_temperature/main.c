/* Uses I2C to read from an AHT20 temperature and humidity sensor. */

#include "../stm32f405.h"

int is_set(int *addr, int value)
{
    return (*addr & value) == value;
}

void I2C_wait_until_not_busy(void)
{
    /* write_string "Wait until busy clear\r\n" */
    I2C *i2c = (I2C *)I2C1;
    while (i2c->sr2 & I2C1_sr2_busy);
}

void I2C_send_start(void)
{
    /* write_string "Start\r\n" */
    /* Generate a start condition and wait for the status bit to be set. */
    I2C *i2c = (I2C *)I2C1;
    i2c->cr1 |= I2C1_cr1_start;
    while ((i2c->sr1 & I2C1_sr1_start) != I2C1_sr1_start);
}

void I2C_send_stop(void)
{
    /* write_string "Stopping\r\n" */
    /* Generate a start condition and wait for the bus to stop being busy. */
    I2C *i2c = (I2C *)I2C1;
    i2c->cr1 |= I2C1_cr1_stop;
    I2C_wait_until_not_busy();
}

int I2C_write_address(int addr, int read_bit)
{
    /* write_string "Write address\r\n" */
    I2C *i2c = (I2C *)I2C1;
    int tmp = i2c->sr1;
    /* Write an address and wait for it to be sent. */
    i2c->dr = (addr << 1) | read_bit;

    int status = i2c->sr1;
    while (status == 0)
        status = i2c->sr1;

    if ((status & I2C1_sr1_addr) != 0) {
        /* Read sr1 then sr2 to clear the address bit in sr1. */
        int s1 = i2c->sr1;
        int s2 = i2c->sr2;
        return 1;
    }

    i2c->sr1 &= ~I2C1_sr1_ackfail;
    I2C_send_stop();
    return 0;
}

void wait_for_byte_sent(void)
{
    I2C *i2c = (I2C *)I2C1;
    while ((i2c->sr1 & I2C1_sr1_txe) == 0);
}

void I2C_start_write(int i2c_addr)
{
    I2C_send_start();
    I2C_write_address(i2c_addr, 0);
}

void I2C_write_byte(int value)
{
    /* Write a byte to the device. */
    I2C *i2c = (I2C *)I2C1;
    i2c->dr = value;
    wait_for_byte_sent();
}

void I2C_start_read(int i2c_addr)
{
    I2C_send_start();
    I2C_write_address(i2c_addr, 1);
}

void I2C_set_ack(void)
{
    I2C *i2c = (I2C *)I2C1;
    i2c->cr1 |= I2C1_cr1_ack;
}

void I2C_clear_ack(void)
{
    I2C *i2c = (I2C *)I2C1;
    i2c->cr1 &= ~I2C1_cr1_ack;
}

int I2C_read_byte(int clear_after)
{
    /* Read a byte from the device. */
    I2C *i2c = (I2C *)I2C1;
    while ((i2c->sr1 & I2C1_sr1_rxne) == 0);
    int value = i2c->dr;

    if (clear_after != 0)
        I2C_clear_ack();

    return value;
}

int I2C_read_byte_of_many(int n, int total)
{
    int pen = total - 2;
    int value = I2C_read_byte(n == pen);
    if (n == pen)
        I2C_send_stop();

    return value;
}

/* Sensor address, commands and status */
#define SensorAddress 0x38
#define GetStatus 0x71
#define Initialize 0xbe
#define TriggerMeasurement 0xac

#define Calibrated 0x08
#define Busy 0x80

int get_status(void)
{
    I2C_start_write(SensorAddress);
    I2C_write_byte(GetStatus);
    I2C_start_read(SensorAddress);
    return I2C_read_byte(0);
}

void get_reading(void)
{
    I2C_start_write(SensorAddress);
    I2C_write_byte(TriggerMeasurement);
    I2C_write_byte(0x33);
    I2C_write_byte(0x00);
    I2C_send_stop();

    /* Wait for 80ms... */
    wait_ms(80);

    int status;
    while (1) {
        status = get_status();
        if ((status & Busy) == 0)
            break;

        wrstr("Busy: ");
        wrhex(status); newline();
    }

    I2C_start_write(SensorAddress);
    I2C_write_byte(GetStatus);
    I2C_start_read(SensorAddress);
    I2C_set_ack();

    status = I2C_read_byte(0);
    int humidity0 = I2C_read_byte(0);
    int humidity1 = I2C_read_byte(0);
    int humidity_temperature = I2C_read_byte(0);
    int temperature1 = I2C_read_byte(0);
    int temperature2 = I2C_read_byte(1);    /* Clear ACK before the last byte. */
    I2C_send_stop();
    int crc = I2C_read_byte(0);

    int humidity2 = humidity_temperature >> 4;
    int temperature0 = humidity_temperature & 0xf;
    int temperature = (temperature0 << 16) | (temperature1 << 8) | temperature2;
    int temp_int = ((temperature * 200) >> 20) - 50;
    /* Find a single digit of the fractional part. */
    int temp_frac = ((temperature * 200) & 0xfffff) / 0x19999;
    write_dec(temp_int); wrch(46); write_dec(temp_frac); newline();
}

/* A systick function is needed when the SysTick timer is enabled. */
void systick(void) {}

void main(void)
{
    setup_system_clock();
    setup_usart();
    setup_i2c();
    start_timer();

    /* Wait for 40ms after power on... */
    wait_ms(40);

    int status = get_status();
    if ((status & Calibrated) == 0) {
        wrstr("Calibrating\r\n");
        I2C_start_write(SensorAddress);
        I2C_write_byte(Initialize);
        I2C_write_byte(0x08);
        I2C_write_byte(0x00);
        I2C_send_stop();
        /* Wait for 10ms... */
        wait_ms(10);
    }

    while (1) {
        get_reading();
        int i = 0;
        while (i < 4) {
            wait_ms(250);
            i++;
        }
    }
}
