#/bin/sh

set -e

LDFLAGS=
#-a

OUT=out.bin

if [ "$1" = "clean" ]; then
    echo "Cleaning..."
    rm *.t $OUT
    exit 1
fi

if [ -z `which tl` ]; then
    echo "Cannot find tl"
    exit 1
fi

5a -t ../l.s
5a -t ../div.s
tc main.c
tc ../clocks.c
tc ../gpio.c
tc ../usart.c
tc ../sys.c
tl $LDFLAGS -l -s -R4 -H0 -T0x08000000 -o $OUT l.t main.t div.t clocks.t gpio.t usart.t sys.t
python3 ../fix_vector_table.py $OUT 0x188
