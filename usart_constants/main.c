#include "../stm32f405.h"

void systick(void)
{
}

extern int getR12(void);
extern int getsp(void);
extern void enablefpu(void);

void main(void)
{
    enablefpu();

    setup_system_clock();

    setup_usart();

    int *i = (int *)0x20002000;
    int *j = (int *)0x30004000;
    int *k = (int *)0x50006000;

    wrhex((int)i); newline();
    wrhex((int)j); newline();
    wrhex((int)k); newline();
    wrhex(*i); newline();

    float *f = (float *)0x20002000;
    float *g = (float *)0x30004000;
    float *h = (float *)0x50006000;

    wrhex((int)f); newline();
    wrhex((int)g); newline();
    wrhex((int)h); newline();

    for (;;) {
    }
}
