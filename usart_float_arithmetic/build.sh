#/bin/sh

set -e

LDFLAGS=-P

OUT=out.bin

if [ "$1" = "clean" ]; then
    echo "Cleaning..."
    rm *.t $OUT
    exit 1
fi

if [ -z `which tl` ]; then
    echo "Cannot find tl"
    exit 1
fi

5a -t ../l.s
5a -t ../div.s
5a -t negate.s
tc main.c
tc ../clocks.c
tc ../gpio.c
tc ../usart.c
tc ../sys.c
tl $LDFLAGS -l -s -R0 -H0 -T0x08000000 -D0x20000000 -o $OUT l.t main.t div.t clocks.t gpio.t usart.t sys.t negate.t
python3 ../fix_vector_table.py $OUT 0x188
