#include "../stm32f405.h"

void systick(void)
{
}

extern int getR12(void);
extern int getsp(void);
extern void enablefpu(void);
extern void dumptmp(int);
extern int dumpfloat(float);
extern float negate(float);

void dump(int addr)
{
    wrstr("r3="); wrhex(*(int *)(addr)); wrch(32);
    wrstr("r7="); wrhex(*(int *)(addr + 4)); wrch(32);
    wrstr("SB="); wrhex(*(int *)(addr + 8)); wrch(32);
    wrstr("SP="); wrhex(*(int *)(addr + 12));
    newline();
}

#define SHOWF(X, E) \
    wrhex(*(int *)(&X)); wrch(' '); wrsfp(X); wrch(' '); wrstr(E); newline();

void main(void)
{
    enablefpu();
    setup_system_clock();
    setup_usart();

    float two = 2.0;
    float three = 3.0;
    float minus_four = -4.0;
    float five = 5.0;

    SHOWF(two, "2.0")
    SHOWF(three, "3.0")
    SHOWF(minus_four, "-4.0")
    SHOWF(five, "5.0")

    float x = negate(two);
    SHOWF(x, "-2.0")
    SHOWF(two, "2.0")
    float y = -two;
    SHOWF(y, "-2.0")

    wrsfp(two * minus_four); wrch(' ');
    wrsfp(two * -minus_four); wrch(' ');
    wrsfp(three * five); newline();

    wrsfp(three - two); wrch(' ');
    wrsfp(-three - two); wrch(' ');
    wrsfp(three - minus_four); newline();

    for (;;) {
    }
}
