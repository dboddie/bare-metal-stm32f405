#include "../thumb2.h"
#include "../stm32f405.h"
#include "../thumb2_fns.h"

void systick(void)
{
}

void usage_fault(int msp)
{
    wrstr("Instruction: "); wrhex(**(int **)(msp + 24)); newline();

    for (;;) {}
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    /* Enable the usage fault exception. */
    *(int *)SHCSR_ADDR |= SHCSR_USGFAULTENA;

    wrstr("MSP="); wrhex(getmsp()); newline();
    wrstr("PSP="); wrhex(getpsp()); newline();
    wrstr("SP="); wrhex(getsp()); newline();
    wrstr("CONTROL="); wrhex(getcontrol()); newline();

    setpsp(0x20010000);

    wrstr("SP="); wrhex(getsp()); newline();

    setcontrol(getcontrol() | CONTROL_SPSEL);

    wrstr("SP="); wrhex(getsp()); newline();

    for (;;) {
    }
}
