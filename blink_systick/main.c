#include "../stm32f405.h"

void start_timer(void)
{
    SysTick *tick = (SysTick *)SYSTICK;
    /* The system clock is 16MHz, so set a reset value for 1s. */
    tick->reload = 16000000 - 1;
    tick->current = 0;
    tick->control = 5;  /* 4=processor clock, 2=SysTick exception, 1=enable */
}

void systick(void)
{
    SysTick *tick = (SysTick *)SYSTICK;
    GPIO *gpioa = (GPIO *)GPIO_A;

    if (tick->control & 0x10000)
        gpioa->odr ^= 1 << 15;
}

#define RCC_AHB1_ENABLE 0x40023830

void main(void)
{
    /* The GPIO pins are on the AHB bus and must be enabled */
    *(uint *)RCC_AHB1_ENABLE |= RCC_AHB1_ENABLE_GPIO_A;

    GPIO *gpioa = (GPIO *)GPIO_A;
    /* Set the pin mode using the pair of bits for PA15: */
    gpioa->moder = GPIO_Output << 30;
    /* Set the speed */
    gpioa->ospeedr = GPIO_HighSpeed << 30;
    /* Turn on the status LED */
    gpioa->odr = 1 << 15;

    start_timer();

    for (;;) {
        systick();
    }
}
