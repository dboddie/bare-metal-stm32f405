#include "../stm32f405.h"

void temp(int x, int y)
{
    int z = x + y;
}

void systick(void)
{
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    const int fck = 42000000;
    const int baud = 115200;
    int f = fck;
    wrhex(f); wrch(32);
    wrhex(baud); wrch(32);
    int x = fck/baud;
    wrhex(fck); wrch(32);
    wrhex(baud); wrch(32);
    wrhex(x); newline();
    int y = fck/baud;
    wrhex(y); newline();
    temp(fck, baud);

    int a = 15;
    int b = 10;
    int c = a % b;
    write_dec(a); wrch(32); write_dec(b); wrch(32); write_dec(c); newline();
    a = -15;
    c = a % b;
    write_dec(a); wrch(32); write_dec(b); wrch(32); write_dec(c); newline();
    a = 15;
    b = -10;
    write_dec(a); wrch(32); write_dec(b); wrch(32); write_dec(c); newline();
    a = -15;
    write_dec(a); wrch(32); write_dec(b); wrch(32); write_dec(c); newline();

    for (;;) {
    }
}
