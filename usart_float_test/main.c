#include "../stm32f405.h"
#include "../thumb2_fns.h"

void systick(void)
{
}

#define SHOWF(X, E) \
    wrhex(*(int *)(&X)); wrch(' '); wrsfp(X); wrch(' '); wrstr(E); newline();

void main(void)
{
    enablefpu();
    setup_system_clock();
    setup_usart();

    /* Assembler format : Vendor format */
    /* MOVF $const, Fd  : VMOV Sd, #imm */
    float x = 0;
    float y = 1.0;
    SHOWF(x, "0.0")
    SHOWF(y, "1.0")

    /* ADDF Fn, Fd      : VADD Sd, Sn, Sm */
    float a = x + y;
    SHOWF(a, "1.0")

    float two = 2.0;
    float three = 3.0;
    float four = 4.0;
    float five = 5.0;
    float point_five = 0.5;
    float ten = 10.0;
    float z = y;

    SHOWF(two, "2.0")
    SHOWF(three, "3.0")
    SHOWF(four, "4.0")
    SHOWF(five, "5.0")
    SHOWF(point_five, "0.5")
    SHOWF(ten, "10.0")
    SHOWF(z, "1.0")

    int *f = (int *)0x20004000;

    /* MOVF Fx, offset(Rn) : VSTR Sn, [Rn, #offset] */
    *(float *)0x20001000 = y;
    *(float *)0x20001004 = 1.0;

    SHOWF(*(float *)0x20001000, "1.0")
    SHOWF(*(float *)0x20001004, "1.0")

    float fs[3];
    fs[0] = x;

    SHOWF(fs[0], "0.0")

    float *fp;
    fp = (float *)0x20002000;
    fp[0] = x;
    fp[1] = 1.0;

    SHOWF(fp[0], "0.0")
    SHOWF(fp[1], "1.0")

    /* MOVF offset(Rn), Fx : VLDR Sn, [Rn, #offset] */
    z = fs[0];
    y = *(float *)0x20001000;
    x = z;

    SHOWF(z, "0.0")
    SHOWF(y, "1.0")
    SHOWF(x, "0.0")

    fs[2] = y;
    SHOWF(fs[2], "1.0")

    fs[2] = z;
    SHOWF(fs[2], "0.0")

    float s = 1.0;
    float t = s;
    *f = s;

    SHOWF(s, "1.0")
    SHOWF(t, "1.0")
    SHOWF(*f, "1.0")

    a = s + ten;
    SHOWF(a, "11.0")

    float eleven = 11.0;
    SHOWF(eleven, "11.0")

    *(int *)0x20003000 = eleven;
    SHOWF(*(int *)0x20003000, "11.0")

    *(float *)0x20001000 = ten;
    *(float *)0x20001004 = 1.0;

    SHOWF(*(int *)0x20001000, "10.0")
    SHOWF(*(int *)0x20001004, "1.0")

    for (;;) {
    }
}
