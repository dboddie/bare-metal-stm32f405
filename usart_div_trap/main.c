#include "../thumb2.h"
#include "../stm32f405.h"
#include "../thumb2_fns.h"

void systick(void)
{
}

void usage_fault(int msp)
{
    wrstr("Instruction: "); wrhex(**(int **)(msp + 24)); newline();

    for (;;) {}
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    /* Enable the usage fault exception. */
    *(int *)SHCSR_ADDR |= SHCSR_USGFAULTENA;

    /* Enable division by zero trapping. */
    *(int *)CCR_ADDR |= CCR_DIV_0_TRP;

    wrstr("CCR="); wrhex(getccr()); newline();
    /* Bit 9 (STKALIGN) appears to be set by default. */

    int a = 1;
    int b = 0;
    a = a / b;

    wrstr("End\r\n");

    for (;;) {
    }
}
