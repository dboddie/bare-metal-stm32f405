/* Include constants */
#include "stm32f405.h"

/* I2C1 can use the PB6,7 pins assigned to SCL1 and SDA1 on the MicroMod when
   configured in alternate function 4 */

void enable_i2c(void)
{
    /* The I2C peripherals are on the APB1 bus and must be enabled.
       See the entry for I2C1 in the memory map in the reference manual. */
    RCC *rcc = (RCC *)RCC_CR;
    rcc->apb1enr |= RCC_APB1_ENABLE_I2C1;
}

void enable_pb67(void)
{
    GPIO *gpiob = (GPIO *)GPIO_B;
    /* Using pins 6 and 7: the low word is written to with 4 bits per pin. */
    gpiob->afrl = (gpiob->afrl & 0x00ffffff) | 0x44000000;
    /* Set the pin modes for pins 6 [SCL1] and 7 [SDA1] to alternate function 4
       as described in the datasheet [stm32f405rg-1851084.pdf]. */
    gpiob->moder = (gpiob->moder & 0xffff0fff) |
                   (GPIO_Alternate << 12) | (GPIO_Alternate << 14);
    /* Set the output type to be open drain: bits 6 and 7 set. */
    gpiob->otyper = (gpiob->otyper & 0xffffff3f) | 0xc0;
    /* Disable pull-up/down resistors. */
    gpiob->pupdr = gpiob->pupdr & 0xffff0fff;
    /* Set the speed: very high speed in bits 12,13 and 14,15. */
    gpiob->ospeedr = (gpiob->ospeedr & 0xffff0fff) |
                     (GPIO_VeryHighSpeed << 12) | (GPIO_VeryHighSpeed << 14);
}

void setup_i2c(void)
{
    enable_i2c();
    /* The I2C1 pins are accessed via GPIO B */
    enable_GPIO_B();

    enable_pb67();

    I2C *i2c = (I2C *)I2C1;
    i2c->cr1 |= I2C1_cr1_swrst;
    i2c->cr1 &= ~I2C1_cr1_swrst;

    /* Register the peripheral clock frequency [42 MHz] with CR2. */
    i2c->cr2 = (i2c->cr2 & ~I2C1_cr2_freq_mask) | 42;

    /* Configure the I2C clock to set the high and low times of the clock.
       For equal times, T_high == T_low, so T_PCLK = T_low/2.
       The example in the reference manual [page 870] uses 100 kHz and an
       FREQ=8 MHz I2C clock in CR2. Since the required time is half the period
       for 100 kHz, the CCR value is 1/200 / 1/8000 = 40 or 8000/200 = 40.
       If FREQ is 42 MHz, for 100 kHz, CCR = 42000/200 = 210. */
    i2c->ccr = 210; /* setting Sm mode = 0 in bit 15 */

    /* Configure the maximum rise time. In Sm mode, the maximum is 1000 ns.
       the TRISE value is 1 + [maximum/T_PCLK]. For 8 MHz, T_PCLK=125 ns.
       For PCLK=42 MHz, TRISE = 1 + [PCLK/1 MHz] = 43. */
    i2c->trise = 43;

    /* Enable the peripheral */
    i2c->cr1 |= I2C1_cr1_pe;
}
