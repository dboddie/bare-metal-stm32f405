#include "../thumb2.h"
#include "../thumb2_fns.h"
#include "../stm32f405.h"

void systick(void)
{
}

void main(void)
{
    setup_system_clock();
    setup_usart();

    wrstr("PC    = "); wrhex(getpc()); newline();
    wrstr("SP    = "); wrhex(getsp()); newline();
    wrstr("CONTROL = "); wrhex(getcontrol()); newline();
    wrstr("APSR  = "); wrhex(getapsr()); newline();
/* Will cause a trap/hang: wrstr("SPSR  = "); wrhex(getspsr()); newline(); */
    wrstr("CPUID = "); wrhex(getcpuid()); newline();
    enablefpu();
    wrstr("CPACR = "); wrhex(getcpacr()); newline();

    wrstr("FPCCR  = "); wrhex(*(int *)FPCCR_ADDR); newline();
    wrstr("FPCAR  = "); wrhex(*(int *)FPCAR_ADDR); newline();
    wrstr("FPDSCR = "); wrhex(*(int *)FPDSCR_ADDR); newline();
    wrstr("MVFR0  = "); wrhex(*(int *)MVFR0_ADDR); newline();
    wrstr("MVFR1  = "); wrhex(*(int *)MVFR1_ADDR); newline();
    wrstr("FPSCR  = "); wrhex(getfpscr()); newline();
    wrstr("CONTROL = "); wrhex(getcontrol()); newline();

    for (;;) {
    }
}
