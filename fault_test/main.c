#include "../thumb2.h"
#include "../thumb2_fns.h"
#include "../stm32f405.h"

void systick(void)
{
}

void nmi(void)
{
    wrstr("NMI\r\n");

    for (;;) {
    }
}

void hard_fault(void)
{
    wrstr("Hard fault\r\n");

    for (;;) {
    }
}

void mem_manage(void)
{
    wrstr("Mem manage\r\n");

    for (;;) {
    }
}

void bus_fault(void)
{
    wrstr("Bus fault\r\n");

    for (;;) {
    }
}

void usage_fault(void)
{
    wrstr("Usage fault\r\n");

    for (;;) {
    }
}

extern void genbadinstr(void);

void main(void)
{
    setup_system_clock();
    setup_usart();

    wrstr("Starting...\r\n");

    /* Enable the usage fault exception. */
    *(int *)SHCSR_ADDR |= SHCSR_USGFAULTENA;
    wrstr("SHCSR="); wrhex(getshcsr()); newline();

    /* Call an assembly language routine containing an invalid instruction and
       catch the exception in one of the above functions. It should be the
       usage fault handler. */
    genbadinstr();

    wrstr("Waiting...\r\n");

    for (;;) {
    }
}
