#include "../thumb2.h"
#include "vectors.s"

THUMB=4
STACK_TOP=0x20020000

TEXT _start(SB), THUMB, $-4

    MOVW    $setR12(SB), R1
    MOVW    R1, R12	/* static base (SB) */
    MOVW    $STACK_TOP, R1
    MOVW    R1, SP

    /* Copy initial values of data from after the end of the text section to
       the beginning of the data section. */
    MOVW    $etext(SB), R1
    MOVW    $bdata(SB), R2
    MOVW    $edata(SB), R3

_start_loop:
    CMP     R3, R2              /* Note the reversal of the operands */
    BGE     _end_start_loop

    MOVW    (R1), R4
    MOVW    R4, (R2)
    ADD     $4, R1
    ADD     $4, R2
    B       _start_loop

_end_start_loop:

    B   ,main(SB)

TEXT _dummy(SB), THUMB, $-4

    B   ,_dummy(SB)

TEXT _systick(SB), THUMB, $0

    BL  ,systick(SB)
    RET

TEXT _nmi(SB), THUMB, $0
    BL ,nmi(SB)
    RET

TEXT _hard_fault(SB), THUMB, $0
    BL ,hard_fault(SB)
    RET

TEXT _mem_manage(SB), THUMB, $0
    BL ,mem_manage(SB)
    RET

TEXT _bus_fault(SB), THUMB, $0
    BL ,bus_fault(SB)
    RET

TEXT _usage_fault(SB), THUMB, $0
    BL ,usage_fault(SB)
    RET

TEXT getR12(SB), THUMB, $-4
    MOVW R12, R0
    RET

TEXT getsp(SB), THUMB, $-4
    MOVW    SP, R0
    RET

TEXT getpc(SB), THUMB, $-4
    MOVW    R14, R0
    RET

TEXT getsc(SB), THUMB, $-4
    MRC     CpSC, 0, R0, C(CpCONTROL), C(0), CpMainctl
    RET

TEXT getspsr(SB), THUMB, $-4
    /* Read SPSR into R0 */
    WORD    $(0x8000f3ef | (1 << 4) | (0 << 24))
    RET

TEXT getapsr(SB), THUMB, $-4
    /* Read APSR into R0 */
    WORD    $(0x8000f3ef | (0 << 4) | (0 << 24))
    RET

TEXT getfpscr(SB), THUMB, $-4
    VMRS(0)
    RET

TEXT introff(SB), THUMB, $-4
    CPS(1, CPS_I)
    RET

TEXT intron(SB), THUMB, $-4
    CPS(0, CPS_I)
    RET

TEXT genbadinstr(SB), THUMB, $-4
    WORD $0xffffffff
    RET
